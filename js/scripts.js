

const SAMPLE_RATE = 44100;

let FPS = 20;
let BUFFER_SIZE = SAMPLE_RATE / FPS;
const LINE_RESOLUTION = 100;
const CIRCLE_RESOLUTION = 400;
const FIRST_FRAME_ONLY = false;
let playing = false;

const canvas = document.getElementById('canvas');
const start = document.getElementById('start');
const stop = document.getElementById('stop');
const log = document.getElementById('log');
const fps = document.getElementById('fps');
const audio = new AudioContext();

fps.value = FPS;
fps.onchange = () => {
    setFPS(fps.value);
}


const vectorscope = new Vectorscope( audio, canvas, {
    buffersize: 512,
    dotAlpha: 1,
    dotThick: 5,
    lineAlpha: 0.3,
    lineThick: 0.3,
    clear: 1,
});
vectorscope.node.connect( audio.destination );

function setFPS(v) {
    FPS = v;
    BUFFER_SIZE = SAMPLE_RATE / FPS;
}

let source;
let buffer;

const shapes = [
    // {
    //     type: 'circle',
    //     radius: 0.3,
    //     x: 0.3,
    //     y: 0.3,
    // },
    // {
    //     type: 'rectangle',
    //     top: -0.7,
    //     right: 0.7,
    //     bottom: 0.7,
    //     left: -0.7,
    // }
    // {
    //     type: 'line',
    //     ax: -0.1,
    //     ay: -0.1,
    //     bx: 0.1,
    //     by: 0.1,
    // }
];

function createBufferFromShapes(buffer) {

    const positions = [];

    for (const shape of shapes) {
        if (shape.type === 'circle') {
            drawCircle(shape)
        } else if (shape.type === 'rectangle') {
            drawRectangle(shape);
        } else if (shape.type === 'curve') {
            drawCurve(shape);
        } else if (shape.type === 'line') {
            drawLine(shape.ax, shape.ay, shape.bx, shape.by, shape.res);
        } else {
            console.error('invalid shape', shape);
        }
    }
    if (positions.length === 0) {
        addPosition(0,0);
    }


    const chan0 = buffer.getChannelData(0);
    const chan1 = buffer.getChannelData(1);
    
    for (let c = 0; c < BUFFER_SIZE; c++) {
        const i = c % positions.length;

        chan0[c] = positions[i].x;
        chan1[c] = positions[i].y;
    }

    return buffer;

    // SUB FUNCTIONS
    
    function drawRectangle(shape) {
        drawLine(shape.left, shape.top, shape.right, shape.top);
        drawLine(shape.right, shape.top, shape.right, shape.bottom);
        drawLine(shape.right, shape.bottom, shape.left, shape.bottom);
        drawLine(shape.left, shape.bottom, shape.left, shape.top);
        reset();
    }

    function drawCircle(shape) {

        const dots = shape.radius * CIRCLE_RESOLUTION;
        for (let i = 0; i <= dots; i++) {
            let angle = i / dots * Math.PI * 2;

            const x = Math.sin(angle) * shape.radius + shape.x;
            const y = Math.cos(angle) * shape.radius + shape.y;
            addPosition(x, y);
        }
        reset();
    }

    function drawCurve(shape) {

        const s = shape.scale || 1;
        const dots = shape.dots || (shape.radius * CIRCLE_RESOLUTION);
        for (let i = 0; i <= dots; i++) {
            let angle = i / dots * Math.PI * 2;
            let { x, y } = shape.curve(angle);
            
            if (shape.rotation) {
                let xp = x * Math.cos(shape.rotation) - y * Math.sin(shape.rotation);
                let yp = x * Math.sin(shape.rotation) + y * Math.cos(shape.rotation);
                x = xp;
                y = yp;
            }

            if (shape.rotationY) {
                let xp = x * Math.cos(shape.rotationY) - y * Math.sin(shape.rotationY);
                let yp = y;
                x = xp;
                y = yp;
            }

            addPosition(x * s * shape.radius, y * s * shape.radius);
        }
        reset();
    }

    function drawLine(ax, ay, bx, by, res) {
        const dx = bx - ax;
        const dy = by - ay;

        if (!res) {
            res = Math.sqrt(dx*dx + dy*dy) * LINE_RESOLUTION;
        }

        for (let i = 0; i <= res; i++) {
            addPosition(
                ax + (i * dx / res),
                ay + (i * dy / res),
            );
        }
    }

    function addPosition(x, y) {
        positions.push({ x, y });
    }

    function reset() {
        addPosition(0,0);
    }


}

function startAudio() {

    setup();
    stopAudio();
    playing = true;
    stop.disabled = false;
    audio.resume();

    playNextFrame();
    audio.destination.channelInterpretation = 'speakers';


    function playNextFrame() {
        if (!playing) {
            return;
        }

        source = audio.createBufferSource();
        source.connect( vectorscope.node );
        buffer = audio.createBuffer(2, BUFFER_SIZE, SAMPLE_RATE);
        source.buffer = buffer;
        createBufferFromShapes(buffer);

        if (FIRST_FRAME_ONLY) {
            source.loop = true;
        } else {
            source.onended = playNextFrame;
        }
        
        source.start();
        update();

    }
}

function stopAudio() {

    if (source) {
        source.onended = null;
        source.stop();
    }

    playing = false;
    stop.disabled = true;
}

function setup() {
    writeLog('No game initialized.');
}

function update() {}

function writeLog(txt) {
    log.innerHTML = `<p>${ txt }</p>${ log.innerHTML }`
}