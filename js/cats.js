
let time = 0;
let cat;

function setup() {
    cat = {
        type: 'curve',
        radius: 1,
        rotation: 0,
        rotationY: 0,
        curve: catCurve,
        scale: 0.002,
    };
    shapes.splice(0);
    writeLog(`Cats setup`);

    shapes.push(cat);
    time = 0;

}

function update() {
    time++;
    cat.rotationY += 0.01;
    // cat.radius -= 0.001;
}
