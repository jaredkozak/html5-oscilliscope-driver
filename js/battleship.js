
function setup() {
    writeLog(`Battleship initialized`);
    shapes.splice(0);

    // make a grid
    let gridSize = 10;
    let interval = 2 / gridSize

    for (let i = 1; i < gridSize; i++) {
        shapes.push({
            type: 'line',
            ax: -1 + interval,
            ay: i * interval - 1,
            bx: 1 - interval,
            by: i * interval - 1,
            res: gridSize,
        });
    }

}

function update() {

}